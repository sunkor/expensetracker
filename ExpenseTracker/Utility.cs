using Android.App;
using Android.Content;

namespace ExpenseTracker
{
    static class Utility
    {
        public static void ShowProgressDialog(Context context, ProgressDialog mProgressDialog)
        {
            if (mProgressDialog == null)
            {
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.SetMessage(context.GetString(Resource.String.loading));
            }

            mProgressDialog.Show();
        }

        public static void HideProgressDialog(ProgressDialog mProgressDialog)
        {
            if (mProgressDialog != null && mProgressDialog.IsShowing)
            {
                mProgressDialog.Hide();
            }
        }
    }
}