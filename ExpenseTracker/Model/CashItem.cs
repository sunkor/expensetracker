using SQLite;
using System;

namespace ExpenseTracker.Model
{
    public class CashItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public CashItemType ItemType { get; set; }

        public DateTime Date { get; set; }
        public string Category { get; set; }
        public double Amount { get; set; }
        public string Notes { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}{2}{3} - {4}", Date.ToString("dd/MM/yyyy"), Category, Environment.NewLine, Amount.ToString("$###,##.00"), Notes);
        }
    }
}