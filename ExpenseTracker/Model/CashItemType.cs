namespace ExpenseTracker.Model
{
    public enum CashItemType
    {
        Unknown = 0,
        Income = 1,
        Expense = 2
    }
}