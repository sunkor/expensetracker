﻿using Android.Widget;
using System.Collections.Generic;
using Android.Views;
using Android.App;
using ExpenseTracker.Model;
using Java.Lang;
using System;

namespace ExpenseTracker.Adapters
{
    public class CashItemArrayAdapter : BaseAdapter<CashItem>
    {
        private Activity activity;
        private List<CashItem> cashItems;

        public CashItemArrayAdapter(Activity activity, List<CashItem> cashItems)
        {
            this.activity = activity;
            this.cashItems = cashItems;
        }

        public override int Count
        {
            get
            {
                return cashItems.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return cashItems[position].ID;
        }

        public override CashItem this[int position]
        {
            get
            {
                return cashItems[position];
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.ListViewItemCashItem, parent, false);

            var cashItem = this[position];

            //Set
            view.FindViewById<TextView>(Resource.Id.tvDate).Text = cashItem.Date.ToString("dd/MM/yyyy");
            view.FindViewById<TextView>(Resource.Id.tvNotes).Text = cashItem.Notes;
            view.FindViewById<TextView>(Resource.Id.tvCategory).Text = cashItem.Category;
            view.FindViewById<TextView>(Resource.Id.tvAmount).Text = cashItem.Amount.ToString("$###,##0.00");
            view.FindViewById<TextView>(Resource.Id.tvCategoryMarker).Text = cashItem.ItemType == CashItemType.Expense ? "E" : "I";
            return view;
        }
    }
}