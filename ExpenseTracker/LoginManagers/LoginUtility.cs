using Android.App;
using Android.Content;
using Android.Content.PM;
using System;
using System.Collections.Generic;
using System.Json;
using System.Threading.Tasks;
using Xamarin.Auth;

namespace ExpenseTracker
{
    public static class LoginUtility
    {
        public const string FB_ACCOUNT = "Facebook";
        private const string FB_REQUEST_URL = "https://graph.facebook.com/me?fields=name,email";

        public const string GOOGLE_ACCOUNT = "Google";
        private const string GOOGLE_REQUEST_URL = "https://www.googleapis.com/plus/v1/people/me/openIdConnect";

        public const string ACCOUNT_NAME = "ExpenseTrackerAccount";
                
        public static bool TryLoginDetailsFetch(out string name, out string email)
        {
            name = string.Empty;
            email = string.Empty;
            Account account = Account;
            var result = false;
            if (account != null)
            {
                account.Properties.TryGetValue("name", out name);
                account.Properties.TryGetValue("email", out email);
                result = true;
            }
            return result;
        }
       
        public static JsonValue Authenticate(string name, Account account)
        {
            string requestUrl = string.Empty;
            if(name == FB_ACCOUNT)
            {
                requestUrl = FB_REQUEST_URL;
            }
            else if(name == GOOGLE_ACCOUNT)
            {
                requestUrl = GOOGLE_REQUEST_URL;
            }

            // Now that we're logged in, make a OAuth2 request to get the user's info.
            var request = new OAuth2Request("GET", new Uri(requestUrl), null, account);
            Response response = request.GetResponseAsync().Result;
            
            var builder = new AlertDialog.Builder(Application.Context);
            string jsonValue = response.GetResponseText();
            var obj = JsonValue.Parse(jsonValue);
            if (!obj.ContainsKey("error"))
            {
                return jsonValue;
            }
            else
            {
                RemoveCredentials();
                return null;
            }
        }

        #region Account Store
        public static void RemoveCredentials()
        {
            var account = Account;
            if (account != null)
            {
                AccountStore.Create(Application.Context).Delete(account, ACCOUNT_NAME);
            }
        }

        public static void SaveCredentials(string source, string name, string email)
        {
            var account = new Account();
            account.Properties.Add("source", source);
            account.Properties.Add("name", name);
            account.Properties.Add("email", email);
            //account.Properties[JSON_VALUE] = jsonValue;
            AccountStore.Create(Application.Context).Save(account, ACCOUNT_NAME);
        }

        public static Account FetchCredentials(string name)
        {
            IEnumerable<Account> accounts = AccountStore.Create(Application.Context).FindAccountsForService(name);
            var enumerator = accounts.GetEnumerator();
            if (enumerator != null)
            {
                while (enumerator.MoveNext())
                {
                    return enumerator.Current;
                }
            }
            return null;
        }

        public static Account Account
        {
            get
            {
                return FetchCredentials(LoginUtility.ACCOUNT_NAME); 
            }
        }
        #endregion

        public static bool IsAppInstalled(Context context, string [] names)
        {
            bool installed = false;
            var pm = context.PackageManager;
            try
            {
                IList<ApplicationInfo> packages = pm.GetInstalledApplications(PackageInfoFlags.MetaData);
                foreach(var package in packages)
                {
                    if (!installed)
                    {
                        string packageName = package.PackageName.ToUpper();
                        foreach (string name in names)
                        {
                            if (packageName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                            {
                                installed = true;
                                break;
                            }
                        }
                    }
                }
                //pm.GetPackageInfo(names, PackageInfoFlags.Activities);
                //installed = true;
            }
            catch (PackageManager.NameNotFoundException)
            {
                installed = false;
            }
            return installed;
        }
    }
}