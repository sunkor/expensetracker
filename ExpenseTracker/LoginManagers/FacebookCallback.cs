using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Xamarin.Facebook;
using static Xamarin.Facebook.GraphRequest;
using Org.Json;

namespace ExpenseTracker
{
    class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
    {
        public Action HandleCancel { get; set; }
        public Action<FacebookException> HandleError { get; set; }
        public Action<TResult> HandleSuccess { get; set; }

        public void OnCancel()
        {
            var c = HandleCancel;
            if (c != null)
                c();
        }

        public void OnError(FacebookException error)
        {
            var c = HandleError;
            if (c != null)
                c(error);
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            var c = HandleSuccess;
            if (c != null)
                c(result.JavaCast<TResult>());
        }
    }

    class FacebookGraphJSONArrayCallback : Java.Lang.Object, IGraphJSONObjectCallback
    {
        //private Context context;
        public Action<JSONObject, GraphResponse> HandleCompleted { get; set; }

        public FacebookGraphJSONArrayCallback()
        {
        }

        //public FacebookGraphJSONArrayCallback(Context context)
        //{
        //    this.context = context;
        //}

        public void OnCompleted(JSONObject jsonObj, GraphResponse p1)
        {
            //string email = jsonObj.GetString("email");
            //String gender = jsonObj.optString("gender");
            //String name = jsonObj.optString("name");
            //String id = jsonObj.optString("id");
            //String user_birthday = jsonObj.optString("user_birthday");
            //String fields = jsonObj.optString("fields");

            //var builder = new AlertDialog.Builder(Application.Context);

            //var fbProfile = Profile.CurrentProfile;
            //if (fbProfile != null)
            //{
            //    var builder = new AlertDialog.Builder(this.context);
            //    builder.SetMessage(string.Format("Facebook login successful for user - {0}, Email - {1}", fbProfile.Name, email));
            //    builder.SetPositiveButton("Ok", (o, e) => { });
            //    builder.Create().Show();
            //}

            var c = HandleCompleted;
            if (c != null)
                c(jsonObj, p1);
        }
    }
}