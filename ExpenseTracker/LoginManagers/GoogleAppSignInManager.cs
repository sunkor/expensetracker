using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Gms.Common.Apis;
using Android.Util;
using Android.Gms.Common;
using Android.Gms.Auth.Api.SignIn;
using Android.Support.V7.App;
using GmsAuth = Android.Gms.Auth.Api;
using Android.Support.V4.App;
using Android;
using System;

namespace ExpenseTracker
{
    class GoogleSignInResultEventArgs : EventArgs
    {
        private GoogleSignInAccount result;

        public GoogleSignInResultEventArgs(GoogleSignInAccount result) : base()
        {
            this.result = result;
        }

        public string Name {  get { return result.DisplayName; } }
        public string Email { get { return result.Email; } }
    }

    class GoogleAppSignInManager : Java.Lang.Object, View.IOnClickListener,
        GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    {
        public EventHandler<GoogleSignInResultEventArgs> OnGoogleSignInResult;
        public EventHandler<EventArgs> OnGoogleSignInButtonClick;

        private FragmentActivity context;
        const string TAG = "MainActivity";
        const int RC_SIGN_IN = 9001;

        private GoogleApiClient mGoogleApiClient;
                
        public GoogleAppSignInManager(FragmentActivity context)
        {
            this.context = context;
        }

        public void OnCreate(Bundle savedInstanceState)
        {
            context.FindViewById(Resource.Id.sign_in_button).SetOnClickListener(this);
            
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
             .RequestEmail()
             //.RequestProfile()
             .Build();
            
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                .EnableAutoManage(context /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .AddApi(GmsAuth.Auth.GOOGLE_SIGN_IN_API, gso)
                .Build();

            SignInButton signInButton = context.FindViewById<SignInButton>(Resource.Id.sign_in_button);
            signInButton.SetSize(SignInButton.SizeStandard);
            signInButton.SetScopes(gso.GetScopeArray());
            //signInButton.Click += (sender, ee) => { SilentSignIn(); };
        }

        public bool SilentSignIn()
        {
            bool isSuccess = false;

            var opr = GmsAuth.Auth.GoogleSignInApi.SilentSignIn(mGoogleApiClient);
            if (opr.IsDone)
            {
                Log.Debug(TAG, "Got cached sign-in");
                var signInResult = (GoogleSignInResult)opr.Get();
                if (signInResult.IsSuccess)
                {
                    var signInAccount = signInResult.SignInAccount;
                    OnGoogleSignInResult.Invoke(this, new GoogleSignInResultEventArgs(signInAccount));
                    isSuccess = signInResult.IsSuccess;
                }
            }

            if(!isSuccess)
            {
                //GmsAuth.Auth.GoogleSignInApi.SignOut(mGoogleApiClient);
                Intent signInIntent = GmsAuth.Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
                context.StartActivityForResult(signInIntent, RC_SIGN_IN);
            }

            return false;
        }

        public void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == RC_SIGN_IN)
            {
                GoogleSignInResult result = GmsAuth.Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                Log.Debug(TAG, "handleSignInResult:" + result.IsSuccess);
                if (result.IsSuccess)
                {
                    var signInAccount = result.SignInAccount;
                    OnGoogleSignInResult.Invoke(this, new GoogleSignInResultEventArgs(signInAccount));
                }
                else
                {
                    //Intent signInIntent = GmsAuth.Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
                    //context.StartActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        }

        public void OnClick(View v)
        {
            OnGoogleSignInButtonClick(this, new EventArgs());
            switch (v.Id)
            {
                case Resource.Id.sign_in_button:
                    SignIn();
                    break;
                //case Resource.Id.sign_out_button:
                //    SignOut();
                //    break;
                //case Resource.Id.disconnect_button:
                //    RevokeAccess();
                //    break;
            }
        }

        private void SignIn()
        {
            Intent signInIntent = GmsAuth.Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
            context.StartActivityForResult(signInIntent, RC_SIGN_IN);
        }

        //private async void SignOut()
        //{
        //    await GmsAuth.Auth.GoogleSignInApi.SignOut(mGoogleApiClient);
        //    UpdateUI(false);
        //}

        //private async void RevokeAccess()
        //{
        //    await GmsAuth.Auth.GoogleSignInApi.RevokeAccess(mGoogleApiClient);
        //    UpdateUI(false);
        //}

        public void OnConnected(Bundle connectionHint)
        {
            Log.Debug(TAG, "onConnected:" + connectionHint);
        }

        public void OnConnectionSuspended(int cause)
        {
            Log.Warn(TAG, "onConnectionSuspended:" + cause);
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            Log.Debug(TAG, "onConnectionFailed:" + result);
        }
    }
}