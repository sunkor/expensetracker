using System.Collections.Generic;
using SQLite;
using System.Threading.Tasks;
using ExpenseTracker.Model;

namespace ExpenseTracker.Store
{
    class SQLiteDBManager
    {
        private string dbPath;

        public SQLiteDBManager(string dbPath)
        {
            this.dbPath = dbPath;
        }

        public string CreateDatabase()
        {
            try
            {
                var connection = new SQLiteConnection(dbPath);
                {
                    connection.CreateTable<CashItem>();
                    return "Database created";
                }
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public async Task<bool> InsertUpdateDataAsync(CashItem data)
        {
            bool bOk = false;
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                var existingData = await db.FindAsync<CashItem>(x => x.ID == data.ID);
                int result = 0;
                if (existingData == null)
                {
                    result = await db.InsertAsync(data);
                }
                else
                {
                    result = await db.UpdateAsync(data);
                }
                //return "Single data file inserted or updated";
                bOk = true;
            }
            catch (SQLiteException)
            {
                //return ex.Message;
                bOk = false;
            }
            return bOk;
        }

        public async Task<bool> DeleteDataAsync(CashItem data)
        {
            bool bOk = false;
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                if (await db.DeleteAsync(data) != 0)
                    bOk = true;
            }
            catch (SQLiteException)
            {
                //return ex.Message;
                bOk = false;
            }
            return bOk;
        }

        public List<CashItem> GetCashItems()
        {
            var cashItemsList = new List<CashItem>();
            var db = new SQLiteConnection(dbPath);
            var enumerator = db.Table<CashItem>().GetEnumerator();
            while (enumerator.MoveNext())
            {
                cashItemsList.Add(enumerator.Current);
            }
            return cashItemsList;
        }

        public int FindNumberRecords()
        {
            var count = FindNumberRecordsAsync().Result;
            return count;
        }

        public async Task<int> FindNumberRecordsAsync()
        {
            int count = -1;
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                // this counts all records in the database, it can be slow depending on the size of the database
                count = await db.ExecuteScalarAsync<int>("SELECT Count(*) FROM CashItem");

                // for a non-parameterless query
                // var count = db.ExecuteScalar<int>("SELECT Count(*) FROM Person WHERE FirstName="Amy");

                return count;
            }
            catch (SQLiteException)
            {
                count = -1;
            }
            return count;
        }
    }
}