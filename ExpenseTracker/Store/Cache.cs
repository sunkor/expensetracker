using System.Collections.Generic;
using ExpenseTracker.Model;
using System.IO;
using ExpenseTracker.Store;
using System;
using System.Threading.Tasks;

namespace ExpenseTracker
{
    static class Cache
    {
        private static SQLiteDBManager _dbManager = null;
        private static Lazy<string> DBPath = new Lazy<string>(() => Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PersonDatabase.db"));

        private static List<CashItem> expenseItems = new List<CashItem>();

        static Cache()
        {
            _dbManager = new SQLiteDBManager(DBPath.Value);
            _dbManager.CreateDatabase();

            //expenseItems = _dbManager.GetCashItems();
            //if (!string.IsNullOrEmpty(cashitems))
            //{
            //    var expenseItems = JsonConvert.DeserializeObject<List<CashItem>>(cashitems);
            //    if (expenseItems != null && expenseItems.Count > 0)
            //    {
            //        ExpenseItems.AddRange(expenseItems);
            //    }
            //}
            //else
            //{
            //    // Read the contents of our asset
            //    AssetManager assets = Application.Context.Assets;
            //    using (StreamReader sr = new StreamReader(assets.Open("cashitems.json")))
            //    {
            //        cashitems = sr.ReadToEnd();
            //    }
            //    var staticExpenseItems = JsonConvert.DeserializeObject<List<CashItem>>(cashitems);
            //    if (staticExpenseItems != null && staticExpenseItems.Count > 0)
            //    {
            //        ExpenseItems.AddRange(staticExpenseItems);
            //    }
            //}
        }

        public static List<CashItem> CashItems
        {
            get
            {
                expenseItems = _dbManager.GetCashItems();
                return expenseItems;
            }
        }

        //public static List<CashItem> RefreshCashItems()
        //{
        //    var items = _dbManager.GetCashItems();
        //    foreach (var item in items)
        //    {
        //        var existingItem = expenseItems.Find(x => x.ID == item.ID);
        //        if (existingItem != null)
        //        {
        //            existingItem.Date = item.Date;
        //            existingItem.Category = item.Category;
        //            existingItem.Amount = item.Amount;
        //            existingItem.Notes = item.Notes;
        //        }
        //        else
        //        {
        //            expenseItems.Add(item);
        //        }
        //    }
        //    return expenseItems;
        //}

        public static async Task AddUpdateCashItemAsync(CashItem item)
        {
            await _dbManager.InsertUpdateDataAsync(item);
            var existingItem = expenseItems.Find(x => x.ID == item.ID);
            if (existingItem != null)
            {
                existingItem.Date = item.Date;
                existingItem.Category = item.Category;
                existingItem.Amount = item.Amount;
                existingItem.Notes = item.Notes;
            }
            else
            {
                expenseItems.Add(item);
            }
        }

        public static async Task DeleteCashItemAsync(CashItem item)
        {
            await _dbManager.DeleteDataAsync(item);
            var existingItem = expenseItems.Find(x => x.ID == item.ID);
            if (existingItem != null)
                expenseItems.Remove(existingItem);
        }

        //public static void StoreCache()
        //{
        //    string expenseItemsJson = JsonConvert.SerializeObject(expenseItems);
        //    IFolder rootFolder = FileSystem.Current.LocalStorage;
        //    IFolder folder = rootFolder.CreateFolderAsync("ExpenseTrackerData", CreationCollisionOption.OpenIfExists).Result;
        //    IFile file = folder.CreateFileAsync("expenseitems.json", CreationCollisionOption.ReplaceExisting).Result;
        //    file.WriteAllTextAsync(expenseItemsJson).Wait();
        //}

        //public static string LoadCache()
        //{
        //    IFolder rootFolder = FileSystem.Current.LocalStorage;
        //    IFolder folder = rootFolder.CreateFolderAsync("ExpenseTrackerData", CreationCollisionOption.OpenIfExists).Result;
        //    IFile file = folder.CreateFileAsync("expenseitems.json", CreationCollisionOption.OpenIfExists).Result;
        //    string data = file.ReadAllTextAsync().Result;
        //    return data;
        //}
    }
}