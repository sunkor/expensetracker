using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ExpenseTracker.Adapters;
using ExpenseTracker.Model;

namespace ExpenseTracker
{
    public class CashItemClickEventArgs : AdapterView.ItemClickEventArgs
    {
        public CashItem CashItem { get; private set; }

        public CashItemClickEventArgs(CashItem cashItem, AdapterView parent, View view, int position, long id) : base(parent, view, position, id)
        {
            this.CashItem = cashItem;
        }
    }
    
    public class CashItemListFragment : Fragment
    {
        private CashItemArrayAdapter _adapter;

        public EventHandler<CashItemClickEventArgs> OnItemClick;

        private View _view = null;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _view = inflater.Inflate(Resource.Layout.ListViewCashItems, container, false);

            var lvCashItem = _view.FindViewById<ListView>(Resource.Id.tvCashItems);

            lvCashItem.ItemClick += (sender, ea) =>
            {
                var item = _adapter[ea.Position];
                OnItemClick.Invoke(sender, new CashItemClickEventArgs(item, ea.Parent, ea.View, ea.Position, ea.Id));
            };

            _adapter = new CashItemArrayAdapter(this.Activity, Cache.CashItems);
            lvCashItem.Adapter = _adapter;

            return _view;
        }

        public override View View
        {
            get { return _view; }
        }

        public void NotifyDataSetChanged()
        {
            //Cache.RefreshCashItems();
            _adapter.NotifyDataSetChanged();
        }
    }
}