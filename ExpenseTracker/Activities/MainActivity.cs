using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Android.Widget;
using ExpenseTracker.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

[assembly: MetaData("com.facebook.sdk.ApplicationId", Value = "@string/facebook_app_id")]
[assembly: MetaData("com.facebook.sdk.ApplicationName", Value = "@string/ApplicationName")]
namespace ExpenseTracker
{
    [Activity(Label = "Track Expense", MainLauncher = true, Icon = "@drawable/Coin100")]
    public class MainActivity : Activity
    {
        //private TextView txtWelcomeMsg;
        private Button btnAddIncome;
        private Button btnAddExpense;
        private List<string> incomeCategories = null;
        private List<string> expenseCategories = null;

        private CashItemListFragment _cashItemListFragment = null;
        private static string CASH_ITEM_LIST_TAG = "CashItemList";

        private SummaryFragment _summaryFragment = null;
        private static string SUMMARY_FRAGMENT_TAG = "SummaryFragment";
        
        private ProgressDialog mProgressDialog = null;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Window.RequestFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ActivityMain);

            this.ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            
            _cashItemListFragment = new CashItemListFragment();
            _cashItemListFragment.OnItemClick += (sender, ea) =>
            {
                ShowCashItemActivityDialog(string.Empty, ea.CashItem,
                    ea.CashItem.ItemType == CashItemType.Expense ? ExpenseCategories : IncomeCategories);

            };
            var tab = this.ActionBar.NewTab();
            tab.SetText("List");
            tab.TabSelected += (sender, ea) =>
            {
                Fragment frag = FragmentManager.FindFragmentById(Resource.Id.fragment_container);
                if (frag is SummaryFragment)
                {
                    var fragmentTransaction = FragmentManager.BeginTransaction();
                    fragmentTransaction.Remove(frag).Commit();
                }
                frag = FragmentManager.FindFragmentById(Resource.Id.fragment_container);
                if (!(frag is CashItemListFragment))
                {
                    var fragmentTransaction = FragmentManager.BeginTransaction();
                    fragmentTransaction.Add(Resource.Id.fragment_container, _cashItemListFragment, CASH_ITEM_LIST_TAG).Commit();
                }
            };
            this.ActionBar.AddTab(tab);
            //tab.SetIcon(Resource.Drawable.ic_tab_white);

            _summaryFragment = new SummaryFragment();
                        
            tab = this.ActionBar.NewTab();
            tab.SetText("Summary");

            tab.TabSelected += (sender, ea) => 
            {
                Fragment frag = FragmentManager.FindFragmentById(Resource.Id.fragment_container);
                if (frag is CashItemListFragment)
                {
                    var fragmentTransaction = FragmentManager.BeginTransaction();
                    fragmentTransaction.Remove(frag).Commit();
                }
                frag = FragmentManager.FindFragmentById(Resource.Id.fragment_container);
                if (!(frag is SummaryFragment))
                {
                    var fragmentTransaction = FragmentManager.BeginTransaction();
                    fragmentTransaction.Add(Resource.Id.fragment_container, _summaryFragment, SUMMARY_FRAGMENT_TAG).Commit();
                }
            };
            this.ActionBar.AddTab(tab);

            //Utility.ShowProgressDialog(this, mProgressDialog);
            
            //txtWelcomeMsg = (TextView)FindViewById(Resource.Id.txtWelcomeMsg);
            btnAddIncome = (Button)FindViewById(Resource.Id.btnAddIncome);
            btnAddExpense = (Button)FindViewById(Resource.Id.btnAddExpense);

            btnAddExpense.Click += (sender, e) =>
            {
                ShowCashItemActivityDialog("Expense", new CashItem() { ItemType = CashItemType.Expense}, ExpenseCategories);
            };

            btnAddIncome.Click += (sender, e) =>
            {
                ShowCashItemActivityDialog("Income", new CashItem() { ItemType = CashItemType.Income }, IncomeCategories);
            };

            var name = Intent.GetStringExtra("Name");
            var email = Intent.GetStringExtra("Email");
            if (!string.IsNullOrEmpty(name) || (LoginUtility.TryLoginDetailsFetch(out name, out email) && !string.IsNullOrEmpty(name)))
            {
                //this.Title = string.Format("Welcome {0}{1}Track your Expense", name, System.Environment.NewLine);
                //txtWelcomeMsg.Text = string.Format("Welcome {0}", name);
            }
            else
            {
                StartLoginActivity();
            }
            //Utility.HideProgressDialog(mProgressDialog);
        }
        
        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            if(menu == null || menu.Size() == 0)
            {
                MenuInflater.Inflate(Resource.Menu.MainMenu, menu);
            }
            return base.OnPrepareOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if(item.ItemId == Resource.Id.btnLogout)
            {
                RemoveCredentials();
                return true;
            }
            return false;
        }

        private void RemoveCredentials()
        {
            new AlertDialog.Builder(this)
                .SetPositiveButton("Yes", (sender, args) =>
                {
                    LoginUtility.RemoveCredentials();
                    StartActivity(typeof(MainActivity));
                })
                .SetNegativeButton("No", (sender, args) =>
                {
                    // User pressed no 
                })
                .SetMessage("Do you want to Log out ?")
                .SetTitle("Logout")
                .Show();
        }

        private void StartLoginActivity()
        {
            var activity = new Intent(this, typeof(LoginActivity));
            StartActivity(activity);
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            //Cache.StoreCache();
        }

        private void ShowCashItemActivityDialog(string title, CashItem item, List<string> categories)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ItemEntryDialogFragment(item);
            dialogFragment.OnCashItemChanged += (sender, ea) =>
            {
                _cashItemListFragment.NotifyDataSetChanged();
            };
            dialogFragment.OnCashItemDeleted += (sender, ea) =>
            {
                _cashItemListFragment.NotifyDataSetChanged();
            };
            dialogFragment.Arguments = new Bundle();
            dialogFragment.Arguments.PutString("title", title);
            dialogFragment.Arguments.PutStringArrayList("categories", categories);
            dialogFragment.Show(transaction, "dialog_fragment");
        }

        private List<string> ExpenseCategories
        {
            get
            {
                if (expenseCategories == null)
                {
                    string categories = GetJsonData("expense_categories.json");

                    if (!string.IsNullOrEmpty(categories))
                    {
                        expenseCategories = JsonConvert.DeserializeObject<List<string>>(categories);
                    }
                }
                return expenseCategories;
            }
        }

        private List<string> IncomeCategories
        {
            get
            {
                if (incomeCategories == null)
                {
                    string categories = GetJsonData("income_categories.json");

                    if (!string.IsNullOrEmpty(categories))
                    {
                        incomeCategories = JsonConvert.DeserializeObject<List<string>>(categories);
                    }
                }
                return incomeCategories;
            }
        }

        private string GetJsonData(string jsonFile)
        {
            string categories = string.Empty; 
            AssetManager assets = this.Assets;
            using (StreamReader sr = new StreamReader(assets.Open(jsonFile)))
            {
                categories = sr.ReadToEnd();
            }
            return categories;
        }
    }
}