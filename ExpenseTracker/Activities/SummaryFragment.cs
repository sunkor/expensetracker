using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.App;

namespace ExpenseTracker
{
    class SummaryFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.SummaryFragment, container, false);
            SetChartData(view);
            return view;
        }

        private void SetChartData(View view)
        {
            FrameLayout layout = (FrameLayout)view.FindViewById(Resource.Id.sampleexpense);
            layout.AddView(ChartUtility.GetSampleContent(this.Activity, "Expenses", ChartUtility.GetExpenseData()));

            layout = (FrameLayout)view.FindViewById(Resource.Id.sampleincome);
            layout.AddView(ChartUtility.GetSampleContent(this.Activity, "Income", ChartUtility.GetIncomeData()));

            layout = (FrameLayout)view.FindViewById(Resource.Id.samplebarchart);
            layout.AddView(ChartUtility.GetSampleContent(this.Activity, "Unemployment Rate(%)", ChartUtility.GetBarData1(), ChartUtility.GetBarData2()));
        }
    }
}