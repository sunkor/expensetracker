﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using Xamarin.Auth;
using System;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Android.Content;
using Android.Support.V4.App;
using System.Json;

namespace ExpenseTracker
{
    [Activity(Label = "Login")]
    public class LoginActivity : FragmentActivity
    {
        private string loginAccount = string.Empty;
        string[] FACEBOOK_NAMES = new string[] { "com.facebook.android", "com.facebook.katana" };
        private ICallbackManager callbackManager;
        private GoogleAppSignInManager googleAppSignInManager;
        private ProgressDialog mProgressDialog = null;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            bool isFacebookAppInstalled = false;
            
            if (LoginUtility.IsAppInstalled(this, FACEBOOK_NAMES))
            {
                FacebookSdk.SdkInitialize(this);
                SetUpFacebookLoginHandler();
                isFacebookAppInstalled = true;
            }
            
            Window.RequestFeature(WindowFeatures.NoTitle);
            
            SetContentView(Resource.Layout.ActivityLogin);

            //Facebool App SignIn
            if (isFacebookAppInstalled)
            {
                var view = this.LayoutInflater.Inflate(Resource.Layout.ButtonFacebookAppSignIn, null, false);
                var fbAppButton = view.FindViewById<View>(Resource.Id.loginFacebookAppButton);

                var layout = FindViewById<LinearLayout>(Resource.Id.layoutLogin);
                
                float scale = BaseContext.Resources.DisplayMetrics.Density;
                var width = float.Parse(Resources.GetString(Resource.String.fb_loginAppButton_width)) * scale;
                var height = float.Parse(Resources.GetString(Resource.String.fb_loginAppButton_height)) * scale;
                var topMargin = float.Parse(Resources.GetString(Resource.String.fb_loginAppButton_topMargin)) * scale;

                LinearLayout.LayoutParams parameters = new LinearLayout.LayoutParams((int)width, (int)height); //, RelativeLayout.LayoutParams.WRAP_CONTENT);
                parameters.Gravity = GravityFlags.CenterHorizontal | GravityFlags.CenterVertical;
                parameters.TopMargin = (int)topMargin;

                fbAppButton.LayoutParameters = parameters;
                fbAppButton.Click += (sender, ea) =>
                {
                    Utility.ShowProgressDialog(this, mProgressDialog);
                };
                
                layout.AddView(fbAppButton);
            }                 

            //OAuth2 SignIn
            var facebook = FindViewById<Button>(Resource.Id.loginFacebookButton);
            facebook.Click += delegate 
            {
                Utility.ShowProgressDialog(this, mProgressDialog);
                LoginToFacebook(true);
            };

            var googleBtn = FindViewById<Button>(Resource.Id.loginGoogleButton);
            googleBtn.Click += delegate
            {
                Utility.ShowProgressDialog(this, mProgressDialog);
                LoginToGoogle(true);
            };

            //Google App SignIn
            googleAppSignInManager = new GoogleAppSignInManager(this);
            googleAppSignInManager.OnGoogleSignInResult = (sender, ea) =>
            {
                RunOnUiThread(() =>
                {
                    StartMainActivity(LoginUtility.GOOGLE_ACCOUNT, ea.Name, ea.Email);
                });
                
            };
            googleAppSignInManager.OnGoogleSignInButtonClick = (sender, ea) =>
            {
                Utility.ShowProgressDialog(this, mProgressDialog);
            };
            googleAppSignInManager.OnCreate(bundle);
        }

        private void StartMainActivity(string account, string loginDetails)
        {
            var obj = JsonValue.Parse(loginDetails);
            var name = (string)obj["name"];
            var email = (string)obj["email"];
            StartMainActivity(account, name, email);
        }
        
        private void StartMainActivity(string account, string name, string email)
        {
            var activity = new Intent(this, typeof(MainActivity));
            activity.PutExtra("Name", name);
            activity.PutExtra("Email", email);
            LoginUtility.SaveCredentials(account, name, email);
            StartActivity(activity);
            Utility.HideProgressDialog(mProgressDialog);
        }
                
        private void SetUpFacebookLoginHandler()
        {
            callbackManager = CallbackManagerFactory.Create();

            var facebookGraphCallback = new FacebookGraphJSONArrayCallback
                {
                HandleCompleted = (jsonObj, response) =>
                {
                    string json = jsonObj.ToString();
                    Account acc = new Account();
                    StartMainActivity(LoginUtility.FB_ACCOUNT, json);
                }
            };

            var loginCallback = new FacebookCallback<LoginResult>
            {
                HandleSuccess = loginResult =>
                {
                    GraphRequest request = GraphRequest.NewMeRequest(
                        loginResult.AccessToken,
                        facebookGraphCallback);
                        //new FacebookGraphJSONArrayCallback(this));

                    var parameters = new Bundle();
                    request.Parameters = new Bundle();
                    request.Parameters.PutString("fields", "name,email");
                    request.ExecuteAsync();
                }
                 ,
                HandleCancel = () => {
                    var builder = new AlertDialog.Builder(this);
                    builder.SetMessage("Facebook login cancelled");
                    builder.SetPositiveButton("Ok", (o, e) => { });
                    builder.Create().Show();
                },
                HandleError = loginError => {
                    var builder = new AlertDialog.Builder(this);
                    builder.SetMessage("Facebook login in error");
                    builder.SetPositiveButton("Ok", (o, e) => { });
                    builder.Create().Show();
                }
            };

            //LoginManager.Instance.LogInWithReadPermissions(this, new string[]{ "public_profile", "email" });
            LoginManager.Instance.RegisterCallback(callbackManager, loginCallback);
            LoginManager.Instance.LogOut();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if(callbackManager != null)
                callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
            if(googleAppSignInManager != null)
                googleAppSignInManager.OnActivityResult(requestCode, resultCode, data);
        }

        #region OAuth2
        private void LoginToFacebook(bool allowCancel)
        {
            string authorizeUrl = string.Format("https://m.facebook.com/dialog/oauth");

            var auth = new OAuth2Authenticator(
                clientId: "249715732068398",
                scope: "public_profile,email",
                authorizeUrl: new Uri(authorizeUrl),
                redirectUrl: new Uri("http://www.facebook.com/connect/login_success.html"));

            auth.AllowCancel = allowCancel;
            auth.Completed += AuthenticationCompleted;

            loginAccount = LoginUtility.FB_ACCOUNT;
            var intent = auth.GetUI(this);
            StartActivity(intent);
        }

        public void LoginToGoogle(bool allowCancel)
        {
            var auth = new OAuth2Authenticator(
           clientId: "57844856441-pdrk7pm4evstpu7p8hnr07saa4nph688.apps.googleusercontent.com",
           scope: "openid email profile", //3 scopes
           authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
           redirectUrl: new Uri("https://www.xamarin.com/"));

            auth.AllowCancel = allowCancel;
            auth.Completed += AuthenticationCompleted;

            loginAccount = LoginUtility.GOOGLE_ACCOUNT;
            var intent = auth.GetUI(this);
            StartActivity(intent);
        }

        private void AuthenticationCompleted(object sender, AuthenticatorCompletedEventArgs ee)
        {
            if (!ee.IsAuthenticated)
            {
                var builder = new AlertDialog.Builder(this);
                builder.SetMessage("Not Authenticated");
                builder.SetPositiveButton("Ok", (o, e) => { });
                builder.Create().Show();
                return;
            }

            var loginDetails = LoginUtility.Authenticate(loginAccount, ee.Account);
            StartMainActivity(loginAccount, loginDetails);
        }
        #endregion
    }
}

