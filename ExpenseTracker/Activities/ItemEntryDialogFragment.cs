using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using ExpenseTracker.Model;
using System.Threading.Tasks;
using System.Collections.Generic;
using static Android.Views.View;

namespace ExpenseTracker
{
    class CashItemEventArgs : EventArgs
    {
        public CashItem CashItem;
    }

    class ItemEntryDialogFragment : DialogFragment
    {
        private static string AMOUNT_FORMAT = "###,##0.00";

        private Button btnDate;
        private Spinner lvCategories;
        private EditText etAmount;
        private EditText etNotes;
        private TextView tvHeader;
        private IList<string> categories;
        private CashItem _cashItem;
        
        public event EventHandler<CashItemEventArgs> OnCashItemChanged;
        public event EventHandler<CashItemEventArgs> OnCashItemDeleted;

        public CashItem CashItem
        {
            get { return _cashItem; }
        }

        public ItemEntryDialogFragment(CashItem cashItem)
        {
            this._cashItem = cashItem;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);           
        }

        public override void OnStart()
        {
            base.OnStart();
                        
            int width = ViewGroup.LayoutParams.MatchParent;
            int height = ViewGroup.LayoutParams.WrapContent;
            Dialog.Window.SetLayout(width, height);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.DialogCashItemEntry, container, false);
            if (view != null)
            {
                //Dialog.Window.RequestFeature(WindowFeatures.NoTitle);

                tvHeader = view.FindViewById<TextView>(Resource.Id.tvHeader);
                btnDate = view.FindViewById<Button>(Resource.Id.btnDate);
                lvCategories = view.FindViewById<Spinner>(Resource.Id.lvCategories);
                etNotes = view.FindViewById<EditText>(Resource.Id.etNotes);
                etAmount = view.FindViewById<EditText>(Resource.Id.etAmount);

                etNotes.FocusChange += (sender, ea) =>
                {
                    if (ea.HasFocus)
                    {
                        this.Dialog.Window.SetSoftInputMode(SoftInput.StateAlwaysVisible);
                    }
                };

                var title = this.Arguments.GetString("title");

                if (!string.IsNullOrEmpty(title))
                {
                    tvHeader.Text = title;
                }

                categories = this.Arguments.GetStringArrayList("categories");
                if (categories != null && categories.Count > 0)
                {
                    var adapter = new ArrayAdapter<string>(this.Activity, Resource.Drawable.spinner_item, categories);
                    lvCategories.Adapter = adapter;
                }

                if (this._cashItem.ID > 0)
                {
                    BindCashItem(_cashItem);
                    view.FindViewById<Button>(Resource.Id.btnDelete).Visibility = ViewStates.Visible;
                    view.FindViewById<Button>(Resource.Id.btnClose).Visibility = ViewStates.Gone;
                }
                else
                {
                    etAmount.Text = 0.ToString(AMOUNT_FORMAT);
                    DateTime dt = DateTime.Now;
                    SetDate(dt);
                }

                etNotes.EditorAction += (sender, ec) =>
                {
                    if (ec.ActionId == Android.Views.InputMethods.ImeAction.Done)
                    {
                        SubmitForm();
                        return;
                    }
                };

                view.FindViewById<Button>(Resource.Id.btnSave).Click += (sender, ee) =>
                {
                    SubmitForm();
                    return;
                };

                view.FindViewById<Button>(Resource.Id.btnClose).Click += (sender, ee) =>
                {
                    this.Dismiss();
                    return;
                };

                view.FindViewById<Button>(Resource.Id.btnDelete).Click += (sender, ee) =>
                {
                    var builder = new AlertDialog.Builder(this.Activity);
                    builder.SetMessage("Do you want to delete ?");
                    builder.SetNegativeButton("Yes", async (senderEx, ea) => 
                    {
                        await DeleteCashItem();
                        this.Dismiss();
                    });
                    builder.SetPositiveButton("No", (senderEx, ea) => { });
                    builder.Show();
                };

                btnDate.Click += (sender, e) =>
                {
                    DateTime dt = DateTime.Now;
                    var dialog = new DatePickerDialog(this.Activity, new EventHandler<DatePickerDialog.DateSetEventArgs>(OnDateSet), dt.Year, dt.Month - 1, dt.Day);
                    dialog.Show();
                };
            }

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation;
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            SetDate(e.Date);
        }

        private void SetDate(DateTime dt)
        {
            btnDate.Text = dt.ToString("dd MMM yyyy");
        }

        private async Task DeleteCashItem()
        {
            await Cache.DeleteCashItemAsync(_cashItem);
            OnCashItemDeleted.Invoke(this, new CashItemEventArgs() { CashItem = _cashItem });
        }

        private async void SubmitForm()
        {
            var item = CreateCashItem();
            int itemId = item.ID;

            await Cache.AddUpdateCashItemAsync(item);
            OnCashItemChanged.Invoke(this, new CashItemEventArgs()
            {
                CashItem = item
            });
            if(itemId == 0) //new item
            {
                string toastText = string.Format("{0} item created.{1}{2}", 
                    item.ItemType.ToString(), System.Environment.NewLine, item.ToString());
                Toast.MakeText(this.Activity, toastText, ToastLength.Short).Show();
                InitCashItem(item.ItemType);
            }
            else
            {
                this.Dismiss();
            }
        }

        private void InitCashItem(CashItemType itemType)
        {
            _cashItem = new CashItem() { ItemType = itemType, Date = DateTime.Now };
            BindCashItem(_cashItem);
        }

        private void BindCashItem(CashItem item)
        {
            SetDate(_cashItem.Date);
            etAmount.Text = _cashItem.Amount.ToString(AMOUNT_FORMAT);
            etNotes.Text = _cashItem.Notes;
            lvCategories.SetSelection(categories.IndexOf(_cashItem.Category));
            //etAmount.RequestFocus();
            etNotes.RequestFocus();
        }

        private CashItem CreateCashItem()
        {
            _cashItem.Date = Convert.ToDateTime(btnDate.Text);
            _cashItem.Category = lvCategories.SelectedItem.ToString();
            double amount = 0;
            double.TryParse(etAmount.Text, out amount);
            _cashItem.Amount = amount;
            _cashItem.Notes = etNotes.Text;
            return _cashItem;
        }
    }
}