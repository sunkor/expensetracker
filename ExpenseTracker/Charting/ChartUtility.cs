﻿using Android.Content;
using Android.Graphics;
using Android.Views;
using Com.Syncfusion.Charts;
using Com.Syncfusion.Charts.Enums;

namespace ExpenseTracker
{
    public static class ChartUtility
    {
        public static View GetSampleContent(Context context, string title, ObservableArrayList data)
        {
            var chart = new SfChart(context);
            chart.Title.Text = title;
            chart.SetBackgroundColor(Color.Transparent);
            chart.Legend.Visibility = Visibility.Visible;

            PieSeries pieSeries = new PieSeries();
            pieSeries.DataMarker.LabelContent = LabelContent.Percentage;
            pieSeries.DataSource = data;
            pieSeries.LegendIcon = ChartLegendIcon.Rectangle;
            //pieSeries.DataMarkerPosition = CircularSeriesDataMarkerPosition.OutsideExtended;
            pieSeries.DataMarkerPosition = CircularSeriesDataMarkerPosition.Outside;
            pieSeries.StartAngle = 75;
            pieSeries.SmartLabelsEnabled = true;
            pieSeries.EndAngle = 435;
            pieSeries.DataMarker.ShowLabel = true;
            pieSeries.ConnectorType = ConnectorType.Bezier;
            pieSeries.AnimationEnabled = true;
            chart.Series.Add(pieSeries);
            return chart;
        }

        public static ObservableArrayList GetExpenseData()
        {
            var datas = new ObservableArrayList();
            datas.Add(new ChartDataPoint("Entertainment", 225));
            datas.Add(new ChartDataPoint("Shopping", 450));
            datas.Add(new ChartDataPoint("Fitness", 172));
            datas.Add(new ChartDataPoint("Transportation", 300));
            datas.Add(new ChartDataPoint("Education", 150));
            datas.Add(new ChartDataPoint("Electronics", 70));
            datas.Add(new ChartDataPoint("Food", 600));
            datas.Add(new ChartDataPoint("Mortgage", 2000));
            datas.Add(new ChartDataPoint("Rent", 1750));
            return datas;
        }

        public static ObservableArrayList GetIncomeData()
        {
            var datas = new ObservableArrayList();
            datas.Add(new ChartDataPoint("Salary", 6500));
            datas.Add(new ChartDataPoint("Landlord Rent", 1200));
            return datas;
        }

        public static View GetSampleContent(Context context, string title, ObservableArrayList data1, ObservableArrayList data2)
        {
            SfChart chart = new SfChart(context);
            chart.Title.Text = title;
            chart.Title.TextSize = 15;
            chart.SetBackgroundColor(Color.Transparent);
            chart.Legend.Visibility = Visibility.Visible;
            chart.Legend.DockPosition = ChartDock.Bottom;
            chart.Legend.ToggleSeriesVisibility = true;
            chart.ColorModel.ColorPalette = ChartColorPalette.TomatoSpectrum;

            NumericalAxis categoryaxis = new NumericalAxis();
            categoryaxis.Minimum = 2005;
            categoryaxis.Maximum = 2012;
            categoryaxis.Interval = 1;
            categoryaxis.Title.Text = "Years";
            chart.PrimaryAxis = categoryaxis;

            NumericalAxis numericalaxis = new NumericalAxis();
            numericalaxis.Minimum = 3;
            numericalaxis.Maximum = 12;
            numericalaxis.EdgeLabelsDrawingMode = EdgeLabelsDrawingMode.Shift;
            numericalaxis.Interval = 1;
            numericalaxis.Title.Text = "Percentage";
            chart.SecondaryAxis = numericalaxis;

            BarSeries barseries = new BarSeries();
            barseries.DataSource = data1;
            barseries.Label = "India";
            barseries.LegendIcon = ChartLegendIcon.Rectangle;
            barseries.DataMarker.ShowLabel = true;
            barseries.AnimationEnabled = true;

            BarSeries barseries1 = new BarSeries();
            barseries1.DataSource = data2;
            barseries1.Label = "US";
            barseries1.LegendIcon = ChartLegendIcon.Rectangle;
            barseries1.DataMarker.ShowLabel = true;
            barseries1.AnimationEnabled = true;

            chart.Series.Add(barseries);
            chart.Series.Add(barseries1);

            return chart;
        }

        public static ObservableArrayList GetBarData1()
        {
            var datas = new ObservableArrayList();
            datas.Add(new ChartDataPoint(2006, 7.8));
            datas.Add(new ChartDataPoint(2007, 7.2));
            datas.Add(new ChartDataPoint(2008, 6.8));
            datas.Add(new ChartDataPoint(2009, 10.7));
            datas.Add(new ChartDataPoint(2010, 10.8));
            datas.Add(new ChartDataPoint(2011, 9.8));
            return datas;
        }

        public static ObservableArrayList GetBarData2()
        {
            var datas = new ObservableArrayList();
            datas.Add(new ChartDataPoint(2006, 4.8));
            datas.Add(new ChartDataPoint(2007, 4.6));
            datas.Add(new ChartDataPoint(2008, 7.2));
            datas.Add(new ChartDataPoint(2009, 9.3));
            datas.Add(new ChartDataPoint(2010, 9.7));
            datas.Add(new ChartDataPoint(2011, 9.0));
            return datas;
        }
    }
}